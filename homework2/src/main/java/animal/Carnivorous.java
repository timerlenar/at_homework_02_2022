package animal;

import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {

    Carnivorous(String name, int satiety) {
        this.name = name;
        this.satiety = satiety;
    }

    public static boolean eat(Food food) {
        if (food instanceof Meat) {
            satiety += food.getEnergy();
            return true;
        } else {
            return false;
        }
    }

    public abstract String getName();
}
