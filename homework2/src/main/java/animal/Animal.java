package animal;

public abstract class Animal {

    protected String name;
    protected static int satiety;

    public abstract String getName();

    public abstract void eat();

    public boolean swimable() {
        return true;
    }
}

