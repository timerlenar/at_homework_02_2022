package animal;

import animal.Interface.Fly;
import animal.Interface.Run;
import animal.Interface.Swim;
import animal.Interface.Voice;

public class Duck extends Herbivore implements Run, Swim, Voice, Fly {

    public Duck(String name, int satiety) {
        super(name, satiety);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void eat() {

    }

    @Override
    public boolean fly() {
        System.out.println(getName() + " летает");
        return true;
    }

    @Override
    public boolean swim() {
        System.out.println(getName() + " плавает");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println(getName() + " бегает");
        return true;
    }

    @Override
    public String voice() {
        return (getName() + " крякает КряКря!!!");
    }
}
