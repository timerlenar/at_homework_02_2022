package animal;

import animal.Interface.Run;
import animal.Interface.Swim;
import animal.Interface.Voice;

public class Crocodile extends Carnivorous implements Swim, Run, Voice {
    public Crocodile(String name, int satiety) {
        super(name, satiety);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void eat() {

    }

    @Override
    public boolean swim() {
        System.out.println(getName() + " плавает");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println(getName() + " бегает");
        return true;
    }

    @Override
    public String voice() {
        return (getName() + " рычит Гргргргргр!!!");
    }
}
