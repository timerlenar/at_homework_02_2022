package animal;

import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {

    Herbivore(String name, int satiety) {
        this.name = name;
        this.satiety = satiety;
    }

    public static boolean eat(Food food) {
        if (food instanceof Grass) {
            satiety += food.getEnergy();
            return true;
        } else {
            return false;
        }
    }

    public abstract String getName();
}
