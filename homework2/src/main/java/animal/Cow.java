package animal;

import animal.Interface.Run;
import animal.Interface.Voice;

public class Cow extends Herbivore implements Run, Voice {

    public Cow(String name, int satiety) {
        super(name, satiety);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void eat() {

    }

    @Override
    public boolean run() {
        System.out.println(getName() + " бегает");
        return true;
    }

    @Override
    public String voice() {
        return (getName() + " мычит Мууууу!!!");
    }
}
