package food;

public abstract class Food {
    private String name;
    private int energy;

    Food(String name, int energy) {
        this.name = name;
        this.energy = energy;
    }

    public int getEnergy() {
        return energy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}



