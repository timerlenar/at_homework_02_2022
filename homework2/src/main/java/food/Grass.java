package food;

public abstract class Grass extends Food {
    Grass(String name, int energy) {
        super(name, energy);
    }
}
