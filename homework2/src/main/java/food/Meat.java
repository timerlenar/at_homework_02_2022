package food;

public abstract class Meat extends Food {
    Meat(String name, int energy) {
        super(name, energy);
    }
}
