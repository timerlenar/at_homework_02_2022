import animal.*;
import food.*;

import java.util.ArrayList;

public class Zoo {
    public static void main(String[] args) throws wrongFoodExceptions {

        Worker worker = new Worker();
        worker.setName("Василий");

        Cow cow = new Cow("Зорька", 100);
        Fish fish = new Fish("Немо", 100);
        Duck duck = new Duck("Скрутчмакдак", 100);
        Crocodile crocodile = new Crocodile("Данди", 100);
        Lion lion = new Lion("Муфаса", 100);
        Wolf wolf = new Wolf("Акела", 100);

        Beef beef = new Beef("говядина", 50);
        Corn corn = new Corn("кукуруза", 20);
        Hay hay = new Hay("сено", 30);
        Pork pork = new Pork("свинина", 50);
        Wheat wheat = new Wheat("пшеница", 15);
        Mutton mutton = new Mutton("баранина", 40);

        worker.feed(fish, hay);
        worker.feed(lion, beef);
        worker.feed(fish, corn);
        worker.feed(crocodile, pork);
        worker.feed(lion, mutton);
        worker.feed(wolf, beef);
        worker.feed(duck, wheat);
        worker.getVoice(cow);
        worker.getVoice(lion);
        worker.getVoice(crocodile);
        worker.getVoice(duck);
        worker.getVoice(wolf);
        cow.run();
        lion.run();
        fish.swim();
        duck.fly();
        crocodile.run();
        crocodile.swim();

        ArrayList<Animal> lake = new ArrayList<>();
        lake.add(fish);
        lake.add(crocodile);
        lake.add(duck);
        for (Animal s : lake) {
            s.swimable();
            System.out.println(s.getName() + " плавает");
        }
    }
}








