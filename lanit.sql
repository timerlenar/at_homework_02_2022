 --1. Вывести список самолетов с кодами 320, 321, 733;

SELECT AIRCRAFT_CODE  FROM FLIGHTS 
WHERE AIRCRAFT_CODE IN ('320', '321', '733')


--2. Вывести список самолетов с кодом не на 3;

SELECT AIRCRAFT_CODE  FROM FLIGHTS 
WHERE AIRCRAFT_CODE NOT LIKE '3%'

--3. Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;

SELECT * FROM TICKETS t 
WHERE PASSENGER_NAME LIKE '%OLGA%'



--4. Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета;

SELECT * FROM AIRCRAFTS_DATA ad 
WHERE RANGE IN (5600, 5700)
ORDER BY RANGE 
DESC 

--5. Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию;

SELECT CITY, AIRPORT_NAME FROM AIRPORTS_DATA ad 
WHERE CITY = 'Moscow'
ORDER BY AIRPORT_NAME 

--6. Вывести список всех городов без повторов в зоне «Europe»;

SELECT  CITY FROM AIRPORTS_DATA ad 
WHERE TIMEZONE NOT LIKE '%Europe%'

--7.Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%

SELECT BOOK_REF, TOTAL_AMOUNT*0.9 AS amount FROM BOOKINGS b 
WHERE BOOK_REF LIKE '%3A4%'


--8. Вывести все данные по местам в самолете с кодом 320 и классом «Business» 
--строками вида «Данные по месту: номер места 1», «Данные по месту: номер места 2» … и тд

 SELECT SEAT_NO , FARE_CONDITIONS   FROM SEATS
 WHERE AIRCRAFT_CODE = '320' AND FARE_CONDITIONS = 'Business'
 ORDER BY SEAT_NO 
   

--9.         Найти максимальную и минимальную сумму бронирования в 2017 году;

SELECT MAX(TOTAL_AMOUNT), MIN(TOTAL_AMOUNT) FROM BOOKINGS 
WHERE TRUNC(BOOK_DATE) BETWEEN TO_DATE ('2017-01-01', 'yyyy-dd-mm') 
AND TO_DATE('2017-31-12', 'yyyy-dd-mm')





--10.  Найти количество мест во всех самолетах, вывести в разрезе самолетов;

SELECT SUM(COUNT(s.SEAT_NO)) 
FROM AIRCRAFTS_DATA a, SEATS s 
WHERE a.AIRCRAFT_CODE =s.AIRCRAFT_CODE 
GROUP BY a.MODEL 
ORDER BY MODEL 

--11.      Найти количество мест во всех самолетах с учетом типа места, вывести в разрезе самолетов и типа мест;

SELECT a.Model , s.FARE_CONDITIONS, COUNT(s.FARE_CONDITIONS) 
FROM AIRCRAFTS_DATA a, SEATS s 
WHERE a.AIRCRAFT_CODE =s.AIRCRAFT_CODE 
GROUP BY a.Model, s.FARE_CONDITIONS 
ORDER BY MODEL 

--12.      Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11;

SELECT COUNT(t.TICKET_NO) FROM TICKETS t 
WHERE PASSENGER_NAME = 'ALEKSANDR STEPANOV' AND PHONE LIKE '%11'


--13.      Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. Отсортировать по убыванию количества билетов;

SELECT COUNT(TICKET_NO) FROM TICKETS t 
WHERE PASSENGER_NAME LIKE ('ALEKSANDR%')
GROUP BY PASSENGER_NAME
HAVING COUNT(TICKET_NO) > 2000


-- 14.      Вывести дни в сентябре 2017 с количеством рейсов больше 500.

SELECT TRUNC(DATE_DEPARTURE) DATE_DEPARTURE, COUNT(*)
FROM FLIGHTS f
WHERE TRUNC(DATE_DEPARTURE) BETWEEN TO_DATE('01.09.2017', 'dd.mm.yyyy') AND TO_DATE('30.09.2017', 'dd.mm.yyyy')
GROUP BY TRUNC(DATE_DEPARTURE)
HAVING COUNT(*) > 500
ORDER BY DATE_DEPARTURE



--15.      Вывести список городов, в которых несколько аэропортов

SELECT CITY  FROM AIRPORTS_DATA ad 
GROUP BY CITY 
HAVING COUNT(AIRPORT_NAME) > 1 


--16.      Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата

SELECT a.Model , COUNT(s.SEAT_NO) 
FROM AIRCRAFTS_DATA a, SEATS s 
WHERE a.AIRCRAFT_CODE =s.AIRCRAFT_CODE 
GROUP BY a.MODEL 
ORDER BY MODEL 

--17.      Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017
SELECT f.* , ad.city 
FROM FLIGHTS f, AIRPORTS_DATA ad 
WHERE f.ARRIVAL_AIRPORT = ad.AIRPORT_CODE
AND TRUNC(f.DATE_DEPARTURE) BETWEEN TO_DATE('01.09.2017', 'dd.mm.yyyy') AND TO_DATE('30.09.2017', 'dd.mm.yyyy')
AND city = 'Moscow' 
ORDER BY DATE_DEPARTURE 

--18.      Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017

SELECT COUNT(FLIGHT_NO), ad.city
FROM FLIGHTS f, AIRPORTS_DATA ad 
WHERE f.ARRIVAL_AIRPORT = ad.AIRPORT_CODE
AND city = 'Moscow' 
AND f.DATE_DEPARTURE BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')
GROUP BY ad.CITY 

--19.      Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017

SELECT ad.AIRPORT_NAME, TRUNC(DATE_ARRIVAL, 'mm'), count(*)
FROM FLIGHTS f JOIN AIRPORTS_DATA ad ON f.DEPARTURE_AIRPORT = ad.AIRPORT_CODE OR f.ARRIVAL_AIRPORT = ad.AIRPORT_CODE
WHERE ad.CITY = 'Moscow' AND TRUNC(DATE_ARRIVAL) BETWEEN TO_DATE('01.01.2017','dd.mm.yyyy') AND TO_DATE('31.12.2017','dd.mm.yyyy')
GROUP BY ad.AIRPORT_NAME, TRUNC(DATE_ARRIVAL, 'mm')


--20.      Найти все билеты по бронированию на «3A4B»

SELECT BOOK_REF FROM TICKETS t 
WHERE BOOK_REF LIKE '3A4B%'

--21.      Найти все перелеты по бронированию на «3A4B»

SELECT tf.* , t.BOOK_REF
FROM TICKET_FLIGHTS tf, TICKETS t
WHERE tf.TICKET_NO = t.TICKET_NO 
AND BOOK_REF LIKE '3A4B%'

 
 






 
 
 
