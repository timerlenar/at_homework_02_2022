import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik vasya = new Kotik(3000, 100, 100, "мяу", "Вася", "Вискас");

        Kotik boris = new Kotik();
        boris.setWeight(4000);
        boris.setPrettiness(70);
        boris.setName("Борис");
        boris.setMeow("Мурр");
        boris.setSatiety(100);
        boris.setFood("Китекат");

        System.out.print("Создан котик " + vasya.getName() + "\n" + "Вес " + vasya.getWeight() + " г" +
                "\n" + "Милота " + vasya.getPrettiness() + "\n" + "Мяукает " + vasya.getMeow() + "\n" + "Ест " +
                vasya.getFood() + "\n");

        System.out.println("======================================================");

        System.out.print("Создан котик " + boris.getName() + "\n" + "Вес " + boris.getWeight() + " г" +
                "\n" + "Милота  " + boris.getPrettiness() + "\n" + "Мяукает " + boris.getMeow() + "\n" + "Ест " +
                boris.getFood() + "\n");

        System.out.println("======================================================");

        vasya.liveAnotherDay();

        System.out.println("======================================================");

        boris.liveAnotherDay();

        System.out.println("======================================================");

        if (vasya.getMeow().equalsIgnoreCase(boris.getMeow())) {
            System.out.println("Котики мурлычат одинаково");
        } else {
            System.out.println("Котики мурлычат по-разному");
        }
        System.out.println("Создано " + Kotik.getCount() + " котика");
    }
}



