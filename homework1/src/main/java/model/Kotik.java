package model;

public class Kotik {

    private int weight;
    private int prettiness;
    private String meow;
    private String name;
    private String food;
    private static int count = 0;
    private int satiety;

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getMeow() {
        return meow;
    }

    public String getName() {
        return name;
    }

    public static int getCount() {
        return count;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public Kotik() {
        count++;
    }

    public Kotik(int weight, int prettiness, int satiety, String meow, String name, String food) {
        this.weight = weight;
        this.prettiness = prettiness;
        this.satiety = satiety;
        this.meow = meow;
        this.name = name;
        this.food = food;
        count++;
    }

    public void eat(int satiety, String food) {
        this.satiety = satiety;
        System.out.println(name + " поел " + food);
    }

    public void eat(int satiety) {
        this.satiety += satiety;
        System.out.println(name + " поел " + food);
    }

    public void eat() {
        eat(100);
    }

    public void liveAnotherDay() {
        int k = 0;
        for (int i = 1; i <= 24; i++) {
            double method = Math.random() * 5 + 1;
            int method1 = (int) Math.round(method);
            k += 1;
            switch (method1) {
                case (1) -> jump(name);
                case (2) -> sharpenClaws(name);
                case (3) -> sleep(name);
                case (4) -> climb(name);
                case (5) -> lickItself(name);
            }
        }
        System.out.println("                                 " + k + "  итерации");
    }

    public boolean jump(String name) {
        satiety -= 30;
        if (satiety > 0) {
            System.out.println(name + " прыгает");
            return false;
        } else {
            System.out.println(name + " хочет есть. ");
            eat(satiety, food);
            return true;
        }
    }

    public boolean sharpenClaws(String name) {
        satiety -= 15;
        if (satiety > 0) {
            System.out.println(name + " точит когти");
            return false;
        } else {
            System.out.println(name + " хочет есть. ");
            eat(satiety);
            return true;
        }
    }

    public boolean sleep(String name) {
        satiety -= 5;
        if (satiety > 0) {
            System.out.println(name + " спит");
            return false;
        } else {
            System.out.println(name + " хочет есть. ");
            eat();
            return true;
        }
    }

    public boolean climb(String name) {
        satiety -= 50;
        if (satiety > 0) {
            System.out.println(name + " лазает");
            return false;
        } else {
            System.out.println(name + " хочет есть. ");
            eat();
            return true;
        }
    }

    public boolean lickItself(String name) {
        satiety -= 10;
        if (satiety > 0) {
            System.out.println(name + " облизывается");
            return false;
        } else {
            System.out.println(name + " хочет есть. ");
            eat();
            return true;
        }
    }
}
