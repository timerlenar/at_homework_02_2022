import animal.*;

public class Zoo {
    public static void main(String[] args) {

        Volier<Herbivore> herbivoreAviary = new Volier<>(Size.LARGE);
        Cow cow = new Cow("Зорька", 100, Size.EXTRA_LARGE);
        Duck duck = new Duck("Скрутч", 50, Size.SMALL);

        herbivoreAviary.add(cow);
        herbivoreAviary.add(duck);



        Volier<Carnivorous> carnivorousAviary = new Volier<>(Size.MEDIUM);
        Wolf wolf = new Wolf("Акела", 200, Size.MEDIUM);
        Wolf wolf1 = new Wolf("Акела2", 150, Size.SMALL);
        Lion lion = new Lion("Муфаса", 200, Size.LARGE);
        Lion lion1 = new Lion("Шрам", 170, Size.MEDIUM);
        carnivorousAviary.add(wolf);
        carnivorousAviary.add(wolf1);
        carnivorousAviary.add(lion);
        carnivorousAviary.add(lion1);


        System.out.println(herbivoreAviary.getAnimalByName("Скрутч"));
        System.out.println(herbivoreAviary.getAnimalByName("Муфаса"));
        System.out.println(carnivorousAviary.getAnimalByName("Акела"));

        carnivorousAviary.delete(wolf);
        System.out.println(carnivorousAviary.getAnimalByName("Акела"));
    }
}







