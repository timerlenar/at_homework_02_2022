import animal.Animal;
import animal.Size;

import java.util.HashSet;


public class Volier<T> {
    private final HashSet<Animal> animals;
    private final Size size;

    public Volier(Size size) {
        this.animals = new HashSet<>();
        this.size = size;
    }


    public void add(T item) {
        Animal animal = (Animal) item;
        if (size.getIndex() >= (animal).getSize().getIndex()) animals.add(animal);
    }

    public void delete(Animal animal) {
        animals.remove(animal);
    }

    public Animal getAnimalByName(String name) {
        for (Animal animal : animals) {
            if (animal.getName().equals(name)) {
                return animal;
            }
        }
        return null;
    }
}
