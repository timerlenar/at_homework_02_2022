import animal.*;

import animal.Interface.Voice;
import food.Food;

public class Worker {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    void getVoice(Voice animal) {
        System.out.println(animal.voice());
    }

    public void feed(Herbivore animal, Food food) throws wrongFoodExceptions {
        if (Herbivore.eat(food)) {
            System.out.println(animal.getName() + " ест " + food.getName());

        } else {
            throw new wrongFoodExceptions(animal.getName() + " не ест " + food.getName());
        }
    }

    public void feed(Carnivorous animal, Food food) throws wrongFoodExceptions {
        if (Carnivorous.eat(food)) {
            System.out.println(animal.getName() + " ест " + food.getName());

        } else {
            throw new wrongFoodExceptions(animal.getName() + " не ест " + food.getName());
        }
    }
}

