package animal;

import animal.Interface.Swim;


public class Fish extends Herbivore implements Swim {

    public Fish(String name, int satiety, Size size) {
        super(name, satiety, size);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void eat() {

    }

    @Override
    public boolean swim() {
        System.out.println(getName() + " плавает");
        return true;
    }
}