package animal;

import animal.Interface.Run;
import animal.Interface.Voice;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf(String name, int satiety, Size size) {
        super(name, satiety, size);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void eat() {

    }

    @Override
    public boolean run() {
        System.out.println(getName() + " бегает");
        return true;
    }

    @Override
    public String voice() {
        return (getName() + " воет Ууууууу!!!");
    }
}