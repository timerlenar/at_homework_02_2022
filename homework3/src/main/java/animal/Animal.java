package animal;
public abstract class Animal {

    protected String name;
    protected static int satiety;
    protected Size size;

    public abstract String getName();

    public Size getSize() {
        return size;
    }

    public abstract void eat();

    public boolean swimable() {
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        return name.equals(((Animal) obj).getName());
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
