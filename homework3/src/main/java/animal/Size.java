package animal;

public enum Size {
    SMALL(0),
    MEDIUM(1),
    LARGE(2),
    EXTRA_LARGE(3);

    private final int index;

    Size(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
